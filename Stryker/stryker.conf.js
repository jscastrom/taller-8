module.exports = function(config) {
  config.set({
    files: ["js/**/*.js", "test/**/*.js"],
    mutator: "javascript",
    packageManager: "npm",
    reporters: ["event-recorder", "clear-text", "progress", "html"],
    testRunner: "karma",
    transpilers: [],
    testFramework: "jasmine",
    coverageAnalysis: "perTest",
    karma: {
      configFile: "test/config/karma.conf.js"
    },
    mutate: ["js/**/*.js"]
  });
};
